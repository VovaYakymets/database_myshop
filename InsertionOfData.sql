insert into Categories (CategoryName, Description)
values
       ('Beverages', 'Soft drinks, coffees, teas, beers, and ales'),
       ('Condiments', 'Sweet and savory sauces, relishes, spreads, and seasonings'),
       ('Confections', 'Desserts, candies, and sweet breads'),
       ('Dairy Products', 'Cheeses'),
       ('Grains/Cereals', 'Breads, crackers, pasta, and cereal');
go

insert into Suppliers (SupplierName, City, Country)
values
       ('Exotic Liquid', 'London', 'UK'),
       ('New Orleans Cajun Delights', 'New Orleans', 'USA'),
       ('Grandma Kelly’s Homestead', 'Ann Arbor', 'USA'),
       ('Tokyo Traders', 'Tokyo', 'Japan'),
       ('Cooperativa de Quesos ‘Las Cabras’', 'Oviedo', 'Spain');
go

insert into Products (ProductName, SupplierId, CategoryId, Price)
values
       ('Chais',1, 1, 18.00),
       ('Chang', 1, 1, 19.00),
       ('Aniseed Syrup', 1, 2, 10.0),
       ('Chef Anton’s Cajun Seasoning', 2, 2, 22.00),
       ('Chef Anton’s Gumbo Mix', 2, 2, 21.35);
go



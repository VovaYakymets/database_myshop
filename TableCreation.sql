create table Suppliers
(
    SupplierId int not null primary key identity ,
    SupplierName nvarchar(50) not null ,
    City nvarchar(50) not null ,
    Country nvarchar(50) not null
)
go

create table Categories
(
    CategoryId int not null primary key identity ,
    CategoryName nvarchar(50) not null ,
    Description nvarchar(100) not null
)
go

create table Products
(
    ProductId int not null primary key identity,
    ProductName nvarchar(50) not null,
    SupplierId int not null references Suppliers(SupplierId),
    CategoryId int not null references Categories(CategoryId),
    Price decimal not null
)
go


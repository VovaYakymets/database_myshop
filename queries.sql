select * from Products
where ProductName like 'C%'
go;

select top 1 * from Products
where Price = (select min(Price) from  Products)
go;

select Price from Products
join Suppliers on Suppliers.SupplierId = Products.SupplierId
where Country = 'USA'
go;

select Suppliers.SupplierId, Suppliers.SupplierName, Suppliers.City, Suppliers.Country from Products
join Categories  on Categories.CategoryId = Products.CategoryId
join Suppliers  on Suppliers.SupplierId = Products.SupplierId
where Categories.CategoryName = 'Condiments'
go;

insert into Suppliers (SupplierName, City, Country)
values  ('Norske Meierier', 'Lviv', 'Ukraine')
go;

insert into Products (ProductName, SupplierId, CategoryId, Price)
values  ('Green tea', 6, 1, 10.00)
go;